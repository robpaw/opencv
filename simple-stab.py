
import numpy as np
import cv2
from matplotlib import pyplot as plt

features = np.ndarray(shape=(0, 1, 2), dtype=np.float32) 

cap = cv2.VideoCapture('../data/gopro-gyro-dataset/walk.MP4')

gray_prev = None
M = np.eye(3)

while(True):
    ret, frame = cap.read()

    gray_curr = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    if features.shape[0] < 100:
        corners = cv2.goodFeaturesToTrack(gray_curr, maxCorners=150, qualityLevel=0.1, minDistance=30, blockSize=30)
        features = np.concatenate((features, corners), axis=0)

    if not gray_prev is None:
        corners, status, error = cv2.calcOpticalFlowPyrLK(gray_prev, gray_curr, features, None, winSize=(30, 30), criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.1))

        old_good = features[status == 1].reshape(-1, 1, 2)
        new_good = corners[status == 1].reshape(-1, 1, 2)
        M = cv2.estimateRigidTransform(new_good, old_good, False)
        features = new_good

    for f in features:
        c = f[0].astype(int)
        center = (c[0], c[1])
        cv2.circle(gray_curr, center, 3, (0, 0, 255), -1)

    gray_prev = gray_curr.copy()

    warped = cv2.warpAffine(gray_curr, M[0:2,], gray_curr.shape);

    cv2.imshow('frame', warped);
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
